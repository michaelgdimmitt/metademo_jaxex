defmodule MetademoJaxex.MixProject do
  use Mix.Project

  def project do
    [
      app: :metademo_jaxex,
      version: "0.1.0",
      elixir: "~> 1.7.4",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      releases: releases()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {MetademoJaxex.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.4.9"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_ecto, "~> 4.0"},
      {:ecto_sql, "~> 3.1"},
      {:postgrex, ">= 0.0.0"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      {:credo, "~> 1.1.0", only: [:dev, :test], runtime: false}
    ]
  end

  # ---------------------------------------------------------------------------
  # NOTE: below are defined two releases. The first will build normally and
  #       store files in the default directory.  The second will put the build
  #       in the specified directory and use quiet mode to reduce chatter.
  #
  #       Examples of building the two options:
  #         - MIX_ENV=prod mix release metademo
  #         - MIX_ENV=prod mix release metademo_quiet
  #
  # Source: https://medium.com/@blackode/quick-glance-elixir-1-9-releases-feature-b6dfce233e71
  # ---------------------------------------------------------------------------
  defp releases do
    [
      metademo: [
        include_executables_for: [:unix],
        applications: [runtime_tools: :permanent]
      ],
      metademo_quiet: [
        include_executables_for: [:unix],
        applications: [runtime_tools: :permanent],
        # - specify a different directory for the release:
        path: "./app_releases",
        # - minimize log / console chatter:
        quiet: true
      ]
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
      # test: ["ecto.create --quiet", "ecto.migrate", "test --trace"]
    ]
  end
end
