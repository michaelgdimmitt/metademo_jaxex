defmodule MetademoJaxex.Repo.Migrations.CreateHostAttrs do
  use Ecto.Migration

  def change do
    create table(:host_attrs) do
      add :name, :string, null: false
      add :value, :string, null: false
      add :is_active, :integer, null: false, default: 1
      add :inserted_by, :string, null: false
      add :updated_by, :string, null: false
      add :host_id, references(:hosts, on_delete: :nothing), null: false

      timestamps()
    end

    create unique_index(:host_attrs, [:host_id, :name, :value])
    create index(:host_attrs, [:host_id])
  end
end
