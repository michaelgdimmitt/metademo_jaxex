defmodule MetademoJaxex.Repo.Migrations.CreateDatacenters do
  use Ecto.Migration

  def change do
    create table(:datacenters) do
      add :code, :string, null: false
      add :name, :string, null: false
      add :pattern, :string, null: true
      add :inserted_by, :string, null: false
      add :updated_by, :string, null: false
      add :is_active, :integer, null: false, default: 1

      timestamps()
    end

    create unique_index(:datacenters, [:code, :name])
  end
end
