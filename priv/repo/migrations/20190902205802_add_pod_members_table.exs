defmodule MetademoJaxex.Repo.Migrations.AddPodMembersTable do
  use Ecto.Migration

  def change do
    create table(:pod_members) do
      add :pod_id, references(:pods, on_delete: :nothing), null: false
      add :host_id, references(:hosts, on_delete: :nothing), null: false
      add :is_active, :integer, null: false, default: 1
      add :inserted_by, :string, null: false
      add :updated_by, :string, null: false

      timestamps()
    end
  end

  # create unique_index(:pod_members, [])
end
