defmodule MetademoJaxex.Repo.Migrations.CreateHostRoles do
  use Ecto.Migration

  def change do
    create table(:host_roles) do
      add :name, :string, null: false
      add :description, :string, null: false
      add :is_active, :integer, null: false, default: 1
      add :inserted_by, :string, null: false
      add :updated_by, :string, null: false

      timestamps()
    end

    create unique_index(:host_roles, [:name])
  end
end
