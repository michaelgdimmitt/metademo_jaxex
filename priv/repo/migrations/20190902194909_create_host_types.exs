defmodule MetademoJaxex.Repo.Migrations.CreateHostTypes do
  use Ecto.Migration

  def change do
    create table(:host_types) do
      add :name, :string, null: false
      add :description, :string, null: false
      add :is_active, :integer, null: false, default: 1
      add :inserted_by, :string, null: false
      add :updated_by, :string, null: false

      timestamps()
    end

    create unique_index(:host_types, [:name])
  end
end
