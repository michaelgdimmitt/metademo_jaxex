# MetademoJaxex

1. Create our project

  ```shell
  mix phx.new metademo_jaxex --database postgres --no-html --no-webpack

  # EXAMPLE:
  ➜  elixir mix phx.new metademo_jaxex --database postgres --no-html --no-webpack
  * creating metademo_jaxex/config/config.exs
  * creating metademo_jaxex/config/dev.exs
  * creating metademo_jaxex/config/prod.exs
  * creating metademo_jaxex/config/prod.secret.exs
  * creating metademo_jaxex/config/test.exs
  * creating metademo_jaxex/lib/metademo_jaxex/application.ex
  * creating metademo_jaxex/lib/metademo_jaxex.ex
  * creating metademo_jaxex/lib/metademo_jaxex_web/channels/user_socket.ex
  * creating metademo_jaxex/lib/metademo_jaxex_web/views/error_helpers.ex
  * creating metademo_jaxex/lib/metademo_jaxex_web/views/error_view.ex
  * creating metademo_jaxex/lib/metademo_jaxex_web/endpoint.ex
  * creating metademo_jaxex/lib/metademo_jaxex_web/router.ex
  * creating metademo_jaxex/lib/metademo_jaxex_web.ex
  * creating metademo_jaxex/mix.exs
  * creating metademo_jaxex/README.md
  * creating metademo_jaxex/.formatter.exs
  * creating metademo_jaxex/.gitignore
  * creating metademo_jaxex/test/support/channel_case.ex
  * creating metademo_jaxex/test/support/conn_case.ex
  * creating metademo_jaxex/test/test_helper.exs
  * creating metademo_jaxex/test/metademo_jaxex_web/views/error_view_test.exs
  * creating metademo_jaxex/lib/metademo_jaxex_web/gettext.ex
  * creating metademo_jaxex/priv/gettext/en/LC_MESSAGES/errors.po
  * creating metademo_jaxex/priv/gettext/errors.pot
  * creating metademo_jaxex/lib/metademo_jaxex/repo.ex
  * creating metademo_jaxex/priv/repo/migrations/.formatter.exs
  * creating metademo_jaxex/priv/repo/seeds.exs
  * creating metademo_jaxex/test/support/data_case.ex

  Fetch and install dependencies? [Yn] Y
  * running mix deps.get
  * running mix deps.compile

  We are almost there! The following steps are missing:

      $ cd metademo_jaxex

  Then configure your database in config/dev.exs and run:

      $ mix ecto.create

  Start your Phoenix app with:

      $ mix phx.server

  You can also run your app inside IEx (Interactive Elixir) as:

      $ iex -S mix phx.server

  ➜  elixir 
  ```

2. Create git repo for this project

  ```shell
  cd metademo_jaxex
  git init
  git add .
  git commit -m "Initial commit"
  ```

3. Configure database connection

  ```shell
  cd metademo_jaxex
  vim config/dev.exs config/test.exs

  # Example configuration settings:
  // config/dev.exs
  # Configure your database
  config :metademo_jaxex, MetademoJaxex.Repo,
    username: "postgres",
    password: "YOUR-PW-HERE",
    database: "metademo_jaxex_dev",
    hostname: "localhost",
    port:     5432,
    show_sensitive_data_on_connection_error: true,
    pool_size: 10


  // config/test.exs
  # Configure your database
  config :metademo_jaxex, MetademoJaxex.Repo,
    username: "postgres",
    password: "YOUR-PW-HERE",
    database: "metademo_jaxex_test",
    hostname: "localhost",
    port:     5432,
    pool: Ecto.Adapters.SQL.Sandbox
  ```

4. Setup/Create database

  ```shell
  mix ecto.create

  # Example:
  ➜  metademo_jaxex git:(master) ✗ mix ecto.create
  The database for MetademoJaxex.Repo has been created
  ➜  metademo_jaxex git:(master) ✗ 
  ```

5. Test basic functionality

  ```shell
  mix phx.server

  # with your browser, visit http://localhost:4000
  ```

6. Create a branch for first migration 

  ```shell
  git checkout -b add_host_role_resource
  ```

7. Create host_roles resource

  ```shell
  mix phx.gen.json Metademo HostRole host_roles name:string description:string is_active:integer inserted_by:string updated_by:string

  # EXAMPLE:
  ➜  metademo_jaxex git:(add_host_role_resource) mix phx.gen.json Metademo HostRole host_roles name:string description:string is_active:integer inserted_by:string updated_by:string
  * creating lib/metademo_jaxex_web/controllers/host_role_controller.ex
  * creating lib/metademo_jaxex_web/views/host_role_view.ex
  * creating test/metademo_jaxex_web/controllers/host_role_controller_test.exs
  * creating lib/metademo_jaxex_web/views/changeset_view.ex
  * creating lib/metademo_jaxex_web/controllers/fallback_controller.ex
  * creating lib/metademo_jaxex/metadata/host_role.ex
  * creating priv/repo/migrations/20190902191009_create_host_roles.exs
  * creating lib/metademo_jaxex/metadata.ex
  * injecting lib/metademo_jaxex/metadata.ex
  * creating test/metademo_jaxex/metadata_test.exs
  * injecting test/metademo_jaxex/metadata_test.exs

  Add the resource to your :api scope in lib/metademo_jaxex_web/router.ex:

      resources "/host_roles", HostRoleController, except: [:new, :edit]


  Remember to update your repository by running migrations:

      $ mix ecto.migrate

  ➜  metademo_jaxex git:(add_host_role_resource) ✗ 
  ```

8. Edit the migration file for needed structure

  ```shell
  vim priv/repo/migrations/[xxxxxxx]_create_host_roles.exs

  defmodule MetademoJaxex.Repo.Migrations.AddHostRolesTable do
    use Ecto.Migration

    def change do
      create table(:host_roles) do
        add :name,        :string, null: false
        add :description, :string, null: false
        add :is_active,   :integer, null: false, default: 1
        add :inserted_by,  :string, null: false
        add :updated_by,  :string, null: false

        timestamps()
      end
      create unique_index(:host_roles, [:name])
    end

  end
  ```

9. Run the migration for host roles

  ```shell
  mix ecto.migrate

  # EXAMPLE:
  ➜  metademo_jaxex git:(add_host_role_resource) ✗ mix ecto.migrate
  Compiling 6 files (.ex)
  warning: function MetademoJaxexWeb.Router.Helpers.host_role_path/3 is undefined or private
    lib/metademo_jaxex_web/controllers/host_role_controller.ex:18

  Generated metademo_jaxex app

  15:12:36.282 [info]  == Running 20190902191009 MetademoJaxex.Repo.Migrations.CreateHostRoles.change/0 forward

  15:12:36.285 [info]  create table host_roles

  15:12:36.347 [info]  create index host_roles_name_index

  15:12:36.358 [info]  == Migrated 20190902191009 in 0.0s
  ➜  metademo_jaxex git:(add_host_role_resource) ✗ 
  ```

10. Commit our changes and merge into master

  ```shell
  git add . 
  git commit -m 'create host roles resource'
 
  git checkout master
  git merge add_host_role_resource

  # EXAMPLE:
  ➜  metademo_jaxex git:(add_host_role_resource) ✗ git status
  On branch add_host_role_resource
  Untracked files:
    (use "git add <file>..." to include in what will be committed)

    lib/metademo_jaxex/metadata.ex
    lib/metademo_jaxex/metadata/
    lib/metademo_jaxex_web/controllers/
    lib/metademo_jaxex_web/views/changeset_view.ex
    lib/metademo_jaxex_web/views/host_role_view.ex
    priv/repo/migrations/20190902191009_create_host_roles.exs
    test/metademo_jaxex/
    test/metademo_jaxex_web/controllers/

  nothing added to commit but untracked files present (use "git add" to track)
  ➜  metademo_jaxex git:(add_host_role_resource) ✗ git add .
  ➜  metademo_jaxex git:(add_host_role_resource) ✗ git commit -m 'create host roles resource'
  [add_host_role_resource ac8a99d] create host roles resource
   9 files changed, 422 insertions(+)
   create mode 100644 lib/metademo_jaxex/metadata.ex
   create mode 100644 lib/metademo_jaxex/metadata/host_role.ex
   create mode 100644 lib/metademo_jaxex_web/controllers/fallback_controller.ex
   create mode 100644 lib/metademo_jaxex_web/controllers/host_role_controller.ex
   create mode 100644 lib/metademo_jaxex_web/views/changeset_view.ex
   create mode 100644 lib/metademo_jaxex_web/views/host_role_view.ex
   create mode 100644 priv/repo/migrations/20190902191009_create_host_roles.exs
   create mode 100644 test/metademo_jaxex/metadata_test.exs
   create mode 100644 test/metademo_jaxex_web/controllers/host_role_controller_test.exs
  ➜  metademo_jaxex git:(add_host_role_resource) git checkout master
  Switched to branch 'master'
  ➜  metademo_jaxex git:(master) git merge add_host_role_resource
  Updating 6acaf07..ac8a99d
  Fast-forward
   lib/metademo_jaxex/metadata.ex                                    | 104 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   lib/metademo_jaxex/metadata/host_role.ex                          |  21 +++++++++++++++++++
   lib/metademo_jaxex_web/controllers/fallback_controller.ex         |  22 ++++++++++++++++++++
   lib/metademo_jaxex_web/controllers/host_role_controller.ex        |  43 +++++++++++++++++++++++++++++++++++++++
   lib/metademo_jaxex_web/views/changeset_view.ex                    |  19 ++++++++++++++++++
   lib/metademo_jaxex_web/views/host_role_view.ex                    |  21 +++++++++++++++++++
   priv/repo/migrations/20190902191009_create_host_roles.exs         |  16 +++++++++++++++
   test/metademo_jaxex/metadata_test.exs                             |  72 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   test/metademo_jaxex_web/controllers/host_role_controller_test.exs | 104 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   9 files changed, 422 insertions(+)
   create mode 100644 lib/metademo_jaxex/metadata.ex
   create mode 100644 lib/metademo_jaxex/metadata/host_role.ex
   create mode 100644 lib/metademo_jaxex_web/controllers/fallback_controller.ex
   create mode 100644 lib/metademo_jaxex_web/controllers/host_role_controller.ex
   create mode 100644 lib/metademo_jaxex_web/views/changeset_view.ex
   create mode 100644 lib/metademo_jaxex_web/views/host_role_view.ex
   create mode 100644 priv/repo/migrations/20190902191009_create_host_roles.exs
   create mode 100644 test/metademo_jaxex/metadata_test.exs
   create mode 100644 test/metademo_jaxex_web/controllers/host_role_controller_test.exs
  ➜  metademo_jaxex git:(master) 
  ```

11. Create host type resource

  ```shell
  git checkout -b add_host_type_resource
  mix phx.gen.json Metademo HostType host_types name:string description:string is_active:integer inserted_by:string updated_by:string

  # EXAMPLE:
  ➜  metademo_jaxex git:(master) git checkout -b add_host_type_resource
  Switched to a new branch 'add_host_type_resource'
  ➜  metademo_jaxex git:(add_host_type_resource)

  ➜  metademo_jaxex git:(add_host_type_resource) mix phx.gen.json Metademo HostType host_types name:string description:string is_active:integer inserted_by:string updated_by:string
  The following files conflict with new files to be generated:

    * lib/metademo_jaxex_web/views/changeset_view.ex
    * lib/metademo_jaxex_web/controllers/fallback_controller.ex

  See the --web option to namespace similarly named resources

  Proceed with interactive overwrite? [Yn] Y
  * creating lib/metademo_jaxex_web/controllers/host_type_controller.ex
  * creating lib/metademo_jaxex_web/views/host_type_view.ex
  * creating test/metademo_jaxex_web/controllers/host_type_controller_test.exs
  * creating lib/metademo_jaxex/metademo/host_type.ex
  * creating priv/repo/migrations/20190902191618_create_host_types.exs
  * creating lib/metademo_jaxex/metademo.ex
  * injecting lib/metademo_jaxex/metademo.ex
  * creating test/metademo_jaxex/metademo_test.exs
  * injecting test/metademo_jaxex/metademo_test.exs

  Add the resource to your :api scope in lib/metademo_jaxex_web/router.ex:

      resources "/host_types", HostTypeController, except: [:new, :edit]


  Remember to update your repository by running migrations:

      $ mix ecto.migrate

  ➜  metademo_jaxex git:(add_host_type_resource) ✗ 
  ```

12. Edit host types migration file

  ```shell
  vim priv/repo/migrations/[xxxxxxx]_add_host_types_table.exs

  # Example:
  defmodule MetademoJaxex.Repo.Migrations.AddHostTypesTable do
    use Ecto.Migration

    def change do
      create table(:host_types) do
        add :name,        :string, null: false
        add :description, :string, null: false
        add :is_active,   :integer, null: false, default: 1
        add :inserted_by,  :string, null: false
        add :updated_by,  :string, null: false

        timestamps()
      end
      create unique_index(:host_types, [:name])

    end
  end
  ```

13. Run our migration, commit the changes, merge into master

  ```shell
  mix ecto.migrate
  git add . 
  git commit -m 'create host types resource'

  git checkout master
  git merge add_host_type_resource
  ```


**DID YOU NOTICE THE ERROR/PROMPT WE GOT WHEN ADDING HOST TYPES?**

  ```shell
  The following files conflict with new files to be generated:

    * lib/metademo_jaxex_web/views/changeset_view.ex
    * lib/metademo_jaxex_web/controllers/fallback_controller.ex
  ```

You can compare the contents of these two files from before adding the host type
resource and the files should be the same.  Nothing to worry about here.

14. Create datacenters resource

  ```shell
  git checkout -b add_datacenter_resource
  mix phx.gen.json Metademo Datacenter datacenters code:string name:string pattern:string inserted_by:string updated_by:string is_active:integer

  # EXAMPLE:
  ➜  metademo_jaxex git:(add_datacenter_resource) mix phx.gen.json Metadata Datacenter datacenters code:string name:string pattern:string inserted_by:string updated_by:string
  You are generating into an existing context.

  The MetademoJaxex.Metadata context currently has 6 functions and 1 files in its directory.

    * It's OK to have multiple resources in the same context as long as they are closely related. But if a context grows too large, consider breaking it apart

    * If they are not closely related, another context probably works better

  The fact two entities are related in the database does not mean they belong to the same context.

  If you are not sure, prefer creating a new context over adding to the existing one.

  Would you like to proceed? [Yn] y
  The following files conflict with new files to be generated:

    * lib/metademo_jaxex_web/views/changeset_view.ex
    * lib/metademo_jaxex_web/controllers/fallback_controller.ex

  See the --web option to namespace similarly named resources

  Proceed with interactive overwrite? [Yn] y
  * creating lib/metademo_jaxex_web/controllers/datacenter_controller.ex
  * creating lib/metademo_jaxex_web/views/datacenter_view.ex
  * creating test/metademo_jaxex_web/controllers/datacenter_controller_test.exs
  * creating lib/metademo_jaxex/metadata/datacenter.ex
  * creating priv/repo/migrations/20190902193419_create_datacenters.exs
  * injecting lib/metademo_jaxex/metadata.ex
  * injecting test/metademo_jaxex/metadata_test.exs

  Add the resource to your :api scope in lib/metademo_jaxex_web/router.ex:

      resources "/datacenters", DatacenterController, except: [:new, :edit]


  Remember to update your repository by running migrations:

      $ mix ecto.migrate

  ➜  metademo_jaxex git:(add_datacenter_resource) ✗ 

  **NOTICE THE WARNINGS ABOVE**



  vim priv/repo/migrations/[xxxxxxxxx]_create_datacenters.exs

  defmodule MetademoJaxex.Repo.Migrations.CreateDatacenters do
    use Ecto.Migration

    def change do
      create table(:datacenters) do
        add :code, :string, null: false
        add :name, :string, null: false
        add :pattern, :string, null: false
        add :is_active, :integer, null: false, default: 1
        add :inserted_by, :string, null: false
        add :updated_by, :string, null: false

        timestamps()
      end
      create unique_index(:datacenters, [:code, :name])
    end
  end

  mix ecto.migrate

  # EXAMPLE:
  ➜  metademo_jaxex git:(add_datacenter_resource) ✗ mix ecto.migrate
  Compiling 8 files (.ex)
  warning: function MetademoJaxexWeb.Router.Helpers.datacenter_path/3 is undefined or private
    lib/metademo_jaxex_web/controllers/datacenter_controller.ex:18

  warning: function MetademoJaxexWeb.Router.Helpers.host_role_path/3 is undefined or private
    lib/metademo_jaxex_web/controllers/host_role_controller.ex:18

  warning: function MetademoJaxexWeb.Router.Helpers.host_type_path/3 is undefined or private
    lib/metademo_jaxex_web/controllers/host_type_controller.ex:18

  Generated metademo_jaxex app

  15:37:35.874 [info]  == Running 20190902193419 MetademoJaxex.Repo.Migrations.CreateDatacenters.change/0 forward

  15:37:35.876 [info]  create table datacenters

  15:37:35.888 [info]  create index datacenters_code_name_index

  15:37:35.895 [info]  == Migrated 20190902193419 in 0.0s
  ➜  metademo_jaxex git:(add_datacenter_resource) ✗ 


  git add . 
  git commit -m 'create datacenters resource'

  git checkout master
  git merge add_datacenter_resource
  ```

*NOTE: As we worked through the above branches, you may have noticed output
letting us know that we need to update the lib/metademo_jaxex_web/router.ex file
and insert the needed for our new resources.*

14. Update router file

  ```shell
  git checkout -b revisit_to_add_routes
  vim lib/metademo_jaxex_web/router.ex

  # EXAMPLE:
  ➜  metademo_jaxex git:(revisit_to_add_routes) ✗ cat lib/metademo_jaxex_web/router.ex     
  defmodule MetademoJaxexWeb.Router do
    use MetademoJaxexWeb, :router

    pipeline :api do
      plug :accepts, ["json"]
    end

    scope "/api", MetademoJaxexWeb do
      pipe_through :api

      resources "/host_roles", HostRoleController, except: [:new, :edit]
      resources "/host_types", HostTypeController, except: [:new, :edit]
      resources "/datacenters", DatacenterController, except: [:new, :edit]
    end
  end
  ➜  metademo_jaxex git:(revisit_to_add_routes) ✗ 
  ```

16. Create host resource

  ```shell
  git checkout -b add_host_resource
  mix phx.gen.json Metademo Host hosts hostname:string is_active:integer host_role_id:references:host_roles host_type_id:references:host_types datacenter_id:references:datacenters inserted_by:string updated_by:string uuid:string ip_address:string

  vim priv/repo/migrations/[xxxxxx]_create_hosts.exs

  # EXAMPLE:
  ➜  metademo_jaxex git:(add_host_resource) ✗ cat priv/repo/migrations/20190902200250_create_hosts.exs
  defmodule MetademoJaxex.Repo.Migrations.CreateHosts do
    use Ecto.Migration

    def change do
      create table(:hosts) do
        add :hostname, :string, null: false
        add :is_active, :integer, null: false, default: 1
        add :inserted_by, :string, null: false
        add :updated_by, :string, null: false
        add :uuid, :string, null: false
        add :ip_address, :string, null: false
        add :host_role_id, references(:host_roles, on_delete: :nothing), null: false
        add :host_type_id, references(:host_types, on_delete: :nothing), null: false
        add :datacenter_id, references(:datacenters, on_delete: :nothing), null: false

        timestamps()
      end

      create index(:hosts, [:host_role_id])
      create index(:hosts, [:host_type_id])
      create index(:hosts, [:datacenter_id])
    end
  end
  ➜  metademo_jaxex git:(add_host_resource) ✗ 


  mix ecto.migrate
  git add . 
  git commit -m 'create host resource.'

  git checkout master
  git merge add_host_schema
  ```

17. Create host attrs resource

  ```shell
  git checkout -b add_host_attrs_resource
  mix phx.gen.json Metademo HostAttr host_attrs name:string value:string is_active:integer host_id:references:hosts inserted_by:string updated_by:string

  vim priv/repo/migrations/[xxxxxx]_create_hosts.exs

  # EXAMPLE:
  defmodule MetademoJaxex.Repo.Migrations.CreateHostAttrs do
    use Ecto.Migration

    def change do
      create table(:host_attrs) do
        add :name, :string, null: false
        add :value, :string, null: false
        add :is_active, :integer, null: false, default: 1
        add :inserted_by, :string, null: false
        add :updated_by, :string, null: false
        add :host_id, references(:hosts, on_delete: :nothing), null: false

        timestamps()
      end
      create unique_index(:host_attrs, [:host_id, :name, :value])
      create index(:host_attrs, [:host_id])
    end
  end
  ```

18. Create Pod and Pod Members resource

  ```shell
  mix phx.gen.json Metademo Pod pod name:string physical_podname:string type:string db_host:string db_host2:string sid:string port:string zone:string is_active:integer inserted_by:string updated_by:string

  # EXAMPLE:
  defmodule MetademoJaxex.Repo.Migrations.CreatePod do
    use Ecto.Migration

    def change do
      create table(:pods) do
        add :name, :string, null: false
        add :type, :string, null: false
        add :zone, :string, null: false
        add :is_active, :integer, null: false, default: 1
        add :inserted_by, :string, null: false
        add :updated_by, :string, null: false
        add :datacenter_id, references(:datacenters, on_delete: :nothing), null: false

        timestamps()
      end
      create index(:pods, [:datacenter_id])
      create unique_index(:pods, [:name])
    end
  end


  # Pod Members
  mix phx.gen.json Metademo PodMember pod_members pod_id:integer host_id:integer is_active:integer inserted_by:string updated_by:string
  ```

