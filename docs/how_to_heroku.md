# README

This document will cover deploying to Heroku.


## HEROKU SETUP

1. Install the Heroku CLI (https://devcenter.heroku.com/articles/heroku-cli)

    ```shell
    ➜  metademo_jaxex git:(master) sudo snap install --classic heroku
    [sudo] password for jfhogarty: 
    heroku v7.33.3 from Heroku✓ installed
    ➜  metademo_jaxex git:(master)
    ```
2. Login

    ```shell
    ➜  metademo_jaxex git:(master) heroku login -i
    heroku: Enter your login credentials
    Email: hogihung@gmail.com
    Password: ************
    Logged in as hogihung@gmail.com
    ➜  metademo_jaxex git:(master) 
    ```

3. Create 'app' on Heroku

    ```shell
    heroku create --buildpack "https://github.com/HashNuke/heroku-buildpack-elixir.git"

    # Optional if you have static assets
    heroku buildpacks:add https://github.com/gjaldon/heroku-buildpack-phoenix-static.git

    # Example:
    ➜  metademo_jaxex git:(deploy_heroku) heroku create --buildpack "https://github.com/HashNuke/heroku-buildpack-elixir.git"
    Creating app... done, ⬢ quiet-waters-07362
    Setting buildpack to https://github.com/HashNuke/heroku-buildpack-elixir.git... done
    https://quiet-waters-07362.herokuapp.com/ | https://git.heroku.com/quiet-waters-07362.git
    ➜  metademo_jaxex git:(deploy_heroku) 
    ➜  metademo_jaxex git:(deploy_heroku) heroku buildpacks:add https://github.com/gjaldon/heroku-buildpack-phoenix-static.git
    Buildpack added. Next release on quiet-waters-07362 will use:
      1. https://github.com/HashNuke/heroku-buildpack-elixir.git
      2. https://github.com/gjaldon/heroku-buildpack-phoenix-static.git
    Run git push heroku master to create a new release using these buildpacks.
    ➜  metademo_jaxex git:(deploy_heroku)
    ```

4. Add Postgres Database

    ```shell
    heroku addons:create heroku-postgresql:hobby-dev

    # Example:
    ➜  metademo_jaxex git:(deploy_heroku) ✗ heroku addons:create heroku-postgresql:hobby-dev
    Creating heroku-postgresql:hobby-dev on ⬢ quiet-waters-07362... free
    Database has been created and is available
     ! This database is empty. If upgrading, you can transfer
     ! data from another database with pg:copy
    Created postgresql-flexible-03848 as DATABASE_URL
    Use heroku addons:docs heroku-postgresql to view documentation
    ➜  metademo_jaxex git:(deploy_heroku) ✗
    ```

5. Configure Environment Variables

    ```shell
    # Pool size
    heroku config:set POOL_SIZE=18

    # Phoenix secret key
    mix phx.gen.secret  # copy the value presented

    heroku config:set SECRET_KEY_BASE=[paste-copied-output-from-above-here]
    ```

6. Update the prod.exs file

    ```shell
    config :something, Something.Endpoint,
      http: [port: {:system, "PORT"}],
      url: [scheme: "https", host: "YOUR-APP-NAME.herokuapp.com", port: 443],
      force_ssl: [rewrite_on [:x_forwarded_proto]],
      cache_static_manifest: "priv/static/manifest.json",
      secret_key_base: System.get_env("SECRET_KEY_BASE")

    config :something, Something.Repo,
      adapter: Ecto.Adapters.Postgres,
      url: System.get_env("DATABASE_URL"),
      pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10"),
      ssl: true

    # Comment out line:
    # import_config "prod.secret.exs"
    ```

7. Edit lib/metademo_jaxex_web/channels/user_socket.ex

    **NOTE:** this step may not be needed?

    ```shell
    ## Transports
    transport :websocket, Phoenix.Transports.Websocket,
    timeout: 45_000
    ```

    # UPDATE: I get errors in CI when above is present

8. Create Procfile

    ```shell
    vim Procfile

    # add the following:
    web: MIX_ENV=prod mix phx.server
    ```

9.  Change version of elixir/erlang

    ```shell
    vim elixir_buildpack.config

    # add/edit the following
    erlang_version=21.0

    elixir_version=1.7.4
    # elixir_version=1.9.1
    ```

10. Commit changes and push to Heroku

    ```shell
    git add config/prod.exs Procfile web/channels/user_socket.ex
    git commit -m 'Prep for deploying to heroku'

    git push heroku master
    ```

11. Run migrations and seed some data (seeding optional)

    ```shell
    heroku run "POOL_SIZE=2 mix ecto.migrate"

    heroku run "POOL_SIZE=2 mix run priv/repo/seeds.exs"
    ```

[ElixirCasts - Deploying to Heroku](https://elixircasts.io/deploying-elixir-with-heroku)

