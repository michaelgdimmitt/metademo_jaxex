# HOW TO DOCKER COMPOSE

Below you will find some examples of how to start this repos application using
docker-compose.  Docker-Compose will pull down the needed Postgres image if it
doesn't already exist on your computer.  It will also build the applications
docker image using the supplied Dockerfile in this repo.


```shell
docker-compose up          # fresh install, app stays open in terminal
  -or-
docker-compose up --build  # run when you have made changes to the repo or Dockerfile
  -or-
docker-compose up -d       # app will run in the background
```



```shell
➜  metademo_jaxex git:(using_docker) docker-compose ps
          Name                        Command               State                Ports              
----------------------------------------------------------------------------------------------------
metademojaxex_app_1        sh ./.docker/startup.sh ta ...   Up      0.0.0.0:4000->4000/tcp, 4001/tcp
metademojaxex_postgres_1   docker-entrypoint.sh postgres    Up      0.0.0.0:5432->5432/tcp          
➜  metademo_jaxex git:(using_docker) 


➜  metademo_jaxex git:(using_docker) docker-compose exec app /bin/bash
root@3861228f921d:/metademo-jaxex# 
root@3861228f921d:/metademo-jaxex# ls
Dockerfile  README.md  _build  config  deps  docker-compose.yml  docs  elixir_buildpack.config	lib  mix.exs  mix.lock	priv  test
root@3861228f921d:/metademo-jaxex# echo $MIX_ENV
test
root@3861228f921d:/metademo-jaxex# 
```


```shell
root@d6843281520f:/metademo-jaxex# mix test
==> connection
Compiling 1 file (.ex)
Generated connection app
==> bunt
Compiling 2 files (.ex)
Generated bunt app
==> gettext

{--snip--}

==> phoenix_ecto
Compiling 6 files (.ex)
Generated phoenix_ecto app
==> metademo_jaxex
Compiling 37 files (.ex)
Generated metademo_jaxex app
...............................................................................

Finished in 1.2 seconds
79 tests, 0 failures

Randomized with seed 549682
root@d6843281520f:/metademo-jaxex# 
```

Now let's change from a test environment in our current containerized setup to
the development environment:

```shell
export MIX_ENV=dev
mix ecto.setup    # this will create our database, run the migrations and seed some data
```

Take a peak at things using iex:

```shell
root@2fe68d4b3767:/metademo-jaxex# iex -S mix
Erlang/OTP 21 [erts-10.3.5.6] [source] [64-bit] [smp:6:6] [ds:6:6:10] [async-threads:1] [hipe]

Interactive Elixir (1.7.4) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)> hosts = Repo.all(Host)
[debug] QUERY OK source="hosts" db=5.5ms decode=1.0ms queue=0.6ms
SELECT h0."id", h0."hostname", h0."inserted_by", h0."ip_address", h0."is_active", h0."updated_by", h0."uuid", h0."inserted_at", h0."updated_at", h0."host_role_id", h0."host_type_id", h0."datacenter_id" FROM "hosts" AS h0 []
[
  %MetademoJaxex.Metademo.Host{
    __meta__: #Ecto.Schema.Metadata<:loaded, "hosts">,
    datacenter: #Ecto.Association.NotLoaded<association :datacenter is not loaded>,
    datacenter_id: 1,
    host_attrs: #Ecto.Association.NotLoaded<association :host_attrs is not loaded>,
    host_role: #Ecto.Association.NotLoaded<association :host_role is not loaded>,
    host_role_id: 1,
    host_type: #Ecto.Association.NotLoaded<association :host_type is not loaded>,
    host_type_id: 1,
    hostname: "web.somehost01.com",
    id: 1,
    inserted_at: ~N[2019-10-27 20:08:18],
    inserted_by: "Seeder",
    ip_address: "192.168.1.4",
    is_active: 1,
    pod: #Ecto.Association.NotLoaded<association :pod is not loaded>,
    pod_member: #Ecto.Association.NotLoaded<association :pod_member is not loaded>,
    updated_at: ~N[2019-10-27 20:08:18],
    updated_by: "Seeder",
    uuid: "ABCD-EFGH-IJKL"
  },

{--snip--}

    updated_by: "Seeder",
    uuid: "MNOP-ABCD-EFGH"
  }
]
iex(2)> 
```


Lastly, we will run our app and test it out using our web browser:

```shell
root@2fe68d4b3767:/metademo-jaxex# mix phx.server
[info] Running MetademoJaxexWeb.Endpoint with cowboy 2.6.3 at 0.0.0.0:4000 (http)
[info] Access MetademoJaxexWeb.Endpoint at http://localhost:4000
[info] GET /api/hosts
[debug] Processing with MetademoJaxexWeb.HostController.index/2
  Parameters: %{}
  Pipelines: [:api]
[debug] QUERY OK source="hosts" db=6.7ms decode=1.0ms queue=0.8ms
SELECT h0."id", h0."hostname", h0."inserted_by", h0."ip_address", h0."is_active", h0."updated_by", h0."uuid", h0."inserted_at", h0."updated_at", h0."host_role_id", h0."host_type_id", h0."datacenter_id" FROM "hosts" AS h0 []
[info] Sent 200 in 57ms

# Be sure to visit http://localhost:4000/api/hosts with your browser!
```


When you are done with the docker environment, in a terminal that is set to the
working directory of your projects repo, issue the following command:

```shell
docker-compose down
```

As an alternative, if you have a terminal open where you see the postgres_1  |
or app_1      |  output, you can try using CNTRL+C to quit.

