#! /bin/sh

echo "DB Host: $DB_HOST"
echo "DB Port: $DB_PORT"

# Wait for Postgres
until nc -z -v -w30 $DB_HOST $DB_PORT; do
 echo 'Waiting for Postgres...'
 sleep 1
done
echo "Postgres is up and running!"
