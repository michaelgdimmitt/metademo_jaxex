#! /bin/sh

# Wait for DB services
sh ./.docker/wait-for-services.sh

# Prepare DB 
sh ./.docker/prepare-db.sh

# Ensure container stays running 
tail -f README.md
