defmodule MetademoJaxexWeb.HostControllerTest do
  use MetademoJaxexWeb.ConnCase

  alias MetademoJaxex.Metademo
  # alias MetademoJaxex.Metademo.Host

  @create_attrs %{
    hostname: "some hostname",
    inserted_by: "some inserted_by",
    ip_address: "some ip_address",
    is_active: 42,
    updated_by: "some updated_by",
    uuid: "some uuid"
  }
  # @update_attrs %{
  #   hostname: "some updated hostname",
  #   inserted_by: "some updated inserted_by",
  #   ip_address: "some updated ip_address",
  #   is_active: 43,
  #   updated_by: "some updated updated_by",
  #   uuid: "some updated uuid"
  # }
  # @invalid_attrs %{hostname: nil, inserted_by: nil, ip_address: nil, is_active: nil, updated_by: nil, uuid: nil}

  def fixture(:host) do
    {:ok, host} = Metademo.create_host(@create_attrs)
    host
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all hosts", %{conn: conn} do
      conn = get(conn, Routes.host_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  # describe "create host" do
  #   test "renders host when data is valid", %{conn: conn} do
  #     conn = post(conn, Routes.host_path(conn, :create), host: @create_attrs)
  #     assert %{"id" => id} = json_response(conn, 201)["data"]

  #     conn = get(conn, Routes.host_path(conn, :show, id))

  #     assert %{
  #              "id" => id,
  #              "hostname" => "some hostname",
  #              "inserted_by" => "some inserted_by",
  #              "ip_address" => "some ip_address",
  #              "is_active" => 42,
  #              "updated_by" => "some updated_by",
  #              "uuid" => "some uuid"
  #            } = json_response(conn, 200)["data"]
  #   end

  #   test "renders errors when data is invalid", %{conn: conn} do
  #     conn = post(conn, Routes.host_path(conn, :create), host: @invalid_attrs)
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end

  # describe "update host" do
  #   setup [:create_host]

  #   test "renders host when data is valid", %{conn: conn, host: %Host{id: id} = host} do
  #     conn = put(conn, Routes.host_path(conn, :update, host), host: @update_attrs)
  #     assert %{"id" => ^id} = json_response(conn, 200)["data"]

  #     conn = get(conn, Routes.host_path(conn, :show, id))

  #     assert %{
  #              "id" => id,
  #              "hostname" => "some updated hostname",
  #              "inserted_by" => "some updated inserted_by",
  #              "ip_address" => "some updated ip_address",
  #              "is_active" => 43,
  #              "updated_by" => "some updated updated_by",
  #              "uuid" => "some updated uuid"
  #            } = json_response(conn, 200)["data"]
  #   end

  #   test "renders errors when data is invalid", %{conn: conn, host: host} do
  #     conn = put(conn, Routes.host_path(conn, :update, host), host: @invalid_attrs)
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end

  # describe "delete host" do
  #   setup [:create_host]

  #   test "deletes chosen host", %{conn: conn, host: host} do
  #     conn = delete(conn, Routes.host_path(conn, :delete, host))
  #     assert response(conn, 204)

  #     assert_error_sent 404, fn ->
  #       get(conn, Routes.host_path(conn, :show, host))
  #     end
  #   end
  # end

  # defp create_host(_) do
  #   host = fixture(:host)
  #   {:ok, host: host}
  # end
end
