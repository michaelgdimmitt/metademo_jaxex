defmodule MetademoJaxexWeb.DatacenterControllerTest do
  use MetademoJaxexWeb.ConnCase

  alias MetademoJaxex.Metademo
  alias MetademoJaxex.Metademo.Datacenter

  @create_attrs %{
    code: "some code",
    inserted_by: "some inserted_by",
    is_active: 42,
    name: "some name",
    pattern: "some pattern",
    updated_by: "some updated_by"
  }
  @update_attrs %{
    code: "some updated code",
    inserted_by: "some updated inserted_by",
    is_active: 43,
    name: "some updated name",
    pattern: "some updated pattern",
    updated_by: "some updated updated_by"
  }
  @invalid_attrs %{
    code: nil,
    inserted_by: nil,
    is_active: nil,
    name: nil,
    pattern: nil,
    updated_by: nil
  }

  def fixture(:datacenter) do
    {:ok, datacenter} = Metademo.create_datacenter(@create_attrs)
    datacenter
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all datacenters", %{conn: conn} do
      conn = get(conn, Routes.datacenter_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create datacenter" do
    test "renders datacenter when data is valid", %{conn: conn} do
      conn = post(conn, Routes.datacenter_path(conn, :create), datacenter: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.datacenter_path(conn, :show, id))

      assert %{
               "id" => id,
               "code" => "some code",
               "inserted_by" => "some inserted_by",
               "is_active" => 42,
               "name" => "some name",
               "pattern" => "some pattern",
               "updated_by" => "some updated_by"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.datacenter_path(conn, :create), datacenter: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update datacenter" do
    setup [:create_datacenter]

    test "renders datacenter when data is valid", %{
      conn: conn,
      datacenter: %Datacenter{id: id} = datacenter
    } do
      conn =
        put(conn, Routes.datacenter_path(conn, :update, datacenter), datacenter: @update_attrs)

      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.datacenter_path(conn, :show, id))

      assert %{
               "id" => id,
               "code" => "some updated code",
               "inserted_by" => "some updated inserted_by",
               "is_active" => 43,
               "name" => "some updated name",
               "pattern" => "some updated pattern",
               "updated_by" => "some updated updated_by"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, datacenter: datacenter} do
      conn =
        put(conn, Routes.datacenter_path(conn, :update, datacenter), datacenter: @invalid_attrs)

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete datacenter" do
    setup [:create_datacenter]

    test "deletes chosen datacenter", %{conn: conn, datacenter: datacenter} do
      conn = delete(conn, Routes.datacenter_path(conn, :delete, datacenter))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.datacenter_path(conn, :show, datacenter))
      end
    end
  end

  defp create_datacenter(_) do
    datacenter = fixture(:datacenter)
    {:ok, datacenter: datacenter}
  end
end
