defmodule MetademoJaxex.MetademoTest do
  use MetademoJaxex.DataCase
  alias MetademoJaxex.Repo
  alias MetademoJaxex.Metademo.Pod
  alias MetademoJaxex.Metademo.Host
  alias MetademoJaxex.Metademo.HostRole
  alias MetademoJaxex.Metademo.HostType
  alias MetademoJaxex.Metademo.Datacenter

  require IEx

  alias MetademoJaxex.Metademo

  describe "host_roles" do
    alias MetademoJaxex.Metademo.HostRole

    @valid_attrs %{
      description: "some description",
      inserted_by: "some inserted_by",
      is_active: 42,
      name: "some name",
      updated_by: "some updated_by"
    }
    @update_attrs %{
      description: "some updated description",
      inserted_by: "some updated inserted_by",
      is_active: 43,
      name: "some updated name",
      updated_by: "some updated updated_by"
    }
    @invalid_attrs %{
      description: nil,
      inserted_by: nil,
      is_active: nil,
      name: nil,
      updated_by: nil
    }

    def host_role_fixture(attrs \\ %{}) do
      {:ok, host_role} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Metademo.create_host_role()

      host_role
    end

    test "list_host_roles/0 returns all host_roles" do
      host_role = host_role_fixture()
      assert Metademo.list_host_roles() == [host_role]
    end

    test "get_host_role!/1 returns the host_role with given id" do
      host_role = host_role_fixture()
      assert Metademo.get_host_role!(host_role.id) == host_role
    end

    test "create_host_role/1 with valid data creates a host_role" do
      assert {:ok, %HostRole{} = host_role} = Metademo.create_host_role(@valid_attrs)
      assert host_role.description == "some description"
      assert host_role.inserted_by == "some inserted_by"
      assert host_role.is_active == 42
      assert host_role.name == "some name"
      assert host_role.updated_by == "some updated_by"
    end

    test "create_host_role/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Metademo.create_host_role(@invalid_attrs)
    end

    test "update_host_role/2 with valid data updates the host_role" do
      host_role = host_role_fixture()
      assert {:ok, %HostRole{} = host_role} = Metademo.update_host_role(host_role, @update_attrs)
      assert host_role.description == "some updated description"
      assert host_role.inserted_by == "some updated inserted_by"
      assert host_role.is_active == 43
      assert host_role.name == "some updated name"
      assert host_role.updated_by == "some updated updated_by"
    end

    test "update_host_role/2 with invalid data returns error changeset" do
      host_role = host_role_fixture()
      assert {:error, %Ecto.Changeset{}} = Metademo.update_host_role(host_role, @invalid_attrs)
      assert host_role == Metademo.get_host_role!(host_role.id)
    end

    test "delete_host_role/1 deletes the host_role" do
      host_role = host_role_fixture()
      assert {:ok, %HostRole{}} = Metademo.delete_host_role(host_role)
      assert_raise Ecto.NoResultsError, fn -> Metademo.get_host_role!(host_role.id) end
    end

    test "change_host_role/1 returns a host_role changeset" do
      host_role = host_role_fixture()
      assert %Ecto.Changeset{} = Metademo.change_host_role(host_role)
    end
  end

  describe "host_types" do
    alias MetademoJaxex.Metademo.HostType

    @valid_attrs %{
      description: "some description",
      inserted_by: "some inserted_by",
      is_active: 42,
      name: "some name",
      updated_by: "some updated_by"
    }
    @update_attrs %{
      description: "some updated description",
      inserted_by: "some updated inserted_by",
      is_active: 43,
      name: "some updated name",
      updated_by: "some updated updated_by"
    }
    @invalid_attrs %{
      description: nil,
      inserted_by: nil,
      is_active: nil,
      name: nil,
      updated_by: nil
    }

    def host_type_fixture(attrs \\ %{}) do
      {:ok, host_type} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Metademo.create_host_type()

      host_type
    end

    test "list_host_types/0 returns all host_types" do
      host_type = host_type_fixture()
      assert Metademo.list_host_types() == [host_type]
    end

    test "get_host_type!/1 returns the host_type with given id" do
      host_type = host_type_fixture()
      assert Metademo.get_host_type!(host_type.id) == host_type
    end

    test "create_host_type/1 with valid data creates a host_type" do
      assert {:ok, %HostType{} = host_type} = Metademo.create_host_type(@valid_attrs)
      assert host_type.description == "some description"
      assert host_type.inserted_by == "some inserted_by"
      assert host_type.is_active == 42
      assert host_type.name == "some name"
      assert host_type.updated_by == "some updated_by"
    end

    test "create_host_type/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Metademo.create_host_type(@invalid_attrs)
    end

    test "update_host_type/2 with valid data updates the host_type" do
      host_type = host_type_fixture()
      assert {:ok, %HostType{} = host_type} = Metademo.update_host_type(host_type, @update_attrs)
      assert host_type.description == "some updated description"
      assert host_type.inserted_by == "some updated inserted_by"
      assert host_type.is_active == 43
      assert host_type.name == "some updated name"
      assert host_type.updated_by == "some updated updated_by"
    end

    test "update_host_type/2 with invalid data returns error changeset" do
      host_type = host_type_fixture()
      assert {:error, %Ecto.Changeset{}} = Metademo.update_host_type(host_type, @invalid_attrs)
      assert host_type == Metademo.get_host_type!(host_type.id)
    end

    test "delete_host_type/1 deletes the host_type" do
      host_type = host_type_fixture()
      assert {:ok, %HostType{}} = Metademo.delete_host_type(host_type)
      assert_raise Ecto.NoResultsError, fn -> Metademo.get_host_type!(host_type.id) end
    end

    test "change_host_type/1 returns a host_type changeset" do
      host_type = host_type_fixture()
      assert %Ecto.Changeset{} = Metademo.change_host_type(host_type)
    end
  end

  describe "datacenters" do
    alias MetademoJaxex.Metademo.Datacenter

    @valid_attrs %{
      code: "some code",
      inserted_by: "some inserted_by",
      is_active: 42,
      name: "some name",
      pattern: "some pattern",
      updated_by: "some updated_by"
    }
    @update_attrs %{
      code: "some updated code",
      inserted_by: "some updated inserted_by",
      is_active: 43,
      name: "some updated name",
      pattern: "some updated pattern",
      updated_by: "some updated updated_by"
    }
    @invalid_attrs %{
      code: nil,
      inserted_by: nil,
      is_active: nil,
      name: nil,
      pattern: nil,
      updated_by: nil
    }

    def datacenter_fixture(attrs \\ %{}) do
      {:ok, datacenter} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Metademo.create_datacenter()

      datacenter
    end

    test "list_datacenters/0 returns all datacenters" do
      datacenter = datacenter_fixture()
      assert Metademo.list_datacenters() == [datacenter]
    end

    test "get_datacenter!/1 returns the datacenter with given id" do
      datacenter = datacenter_fixture()
      assert Metademo.get_datacenter!(datacenter.id) == datacenter
    end

    test "create_datacenter/1 with valid data creates a datacenter" do
      assert {:ok, %Datacenter{} = datacenter} = Metademo.create_datacenter(@valid_attrs)
      assert datacenter.code == "some code"
      assert datacenter.inserted_by == "some inserted_by"
      assert datacenter.is_active == 42
      assert datacenter.name == "some name"
      assert datacenter.pattern == "some pattern"
      assert datacenter.updated_by == "some updated_by"
    end

    test "create_datacenter/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Metademo.create_datacenter(@invalid_attrs)
    end

    test "update_datacenter/2 with valid data updates the datacenter" do
      datacenter = datacenter_fixture()

      assert {:ok, %Datacenter{} = datacenter} =
               Metademo.update_datacenter(datacenter, @update_attrs)

      assert datacenter.code == "some updated code"
      assert datacenter.inserted_by == "some updated inserted_by"
      assert datacenter.is_active == 43
      assert datacenter.name == "some updated name"
      assert datacenter.pattern == "some updated pattern"
      assert datacenter.updated_by == "some updated updated_by"
    end

    test "update_datacenter/2 with invalid data returns error changeset" do
      datacenter = datacenter_fixture()
      assert {:error, %Ecto.Changeset{}} = Metademo.update_datacenter(datacenter, @invalid_attrs)
      assert datacenter == Metademo.get_datacenter!(datacenter.id)
    end

    test "delete_datacenter/1 deletes the datacenter" do
      datacenter = datacenter_fixture()
      assert {:ok, %Datacenter{}} = Metademo.delete_datacenter(datacenter)
      assert_raise Ecto.NoResultsError, fn -> Metademo.get_datacenter!(datacenter.id) end
    end

    test "change_datacenter/1 returns a datacenter changeset" do
      datacenter = datacenter_fixture()
      assert %Ecto.Changeset{} = Metademo.change_datacenter(datacenter)
    end
  end

  describe "hosts" do
    alias MetademoJaxex.Metademo.Host

    @valid_attrs %{
      hostname: "some hostname",
      inserted_by: "some inserted_by",
      ip_address: "some ip_address",
      is_active: 42,
      updated_by: "some updated_by",
      uuid: "some uuid"
    }
    @update_attrs %{
      hostname: "some updated hostname",
      inserted_by: "some updated inserted_by",
      ip_address: "some updated ip_address",
      is_active: 43,
      updated_by: "some updated updated_by",
      uuid: "some updated uuid"
    }
    @invalid_attrs %{
      hostname: nil,
      inserted_by: nil,
      ip_address: nil,
      is_active: nil,
      updated_by: nil,
      uuid: nil
    }

    def host_setup() do
      host_role =
        host_role_fixture(%{
          name: "Web Server",
          description: "use for hosts that are a web server",
          is_active: 1,
          inserted_by: "Seeder",
          updated_by: "Seeder"
        })

      host_type =
        host_type_fixture(%{
          name: "VIRTUAL",
          description: "use this type for virtual machines type hosts",
          is_active: 1,
          inserted_by: "Seeder",
          updated_by: "Seeder"
        })

      datacenter =
        datacenter_fixture(%{
          code: "AUS",
          name: "Austin",
          is_active: 1,
          pattern: "",
          inserted_by: "Seeder",
          updated_by: "Seeder"
        })

      %{datacenter_id: datacenter.id, host_role_id: host_role.id, host_type_id: host_type.id}
    end

    def host_fixture(attrs \\ %{}) do
      # IEx.pry

      {:ok, host} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Map.merge(host_setup())
        |> Metademo.create_host()

      host
    end

    test "list_hosts/0 returns all hosts" do
      host = host_fixture()
      assert Metademo.list_hosts() == [host]
    end

    test "get_host!/1 returns the host with given id" do
      host = host_fixture()
      assert Metademo.get_host!(host.id) == host
    end

    test "create_host/1 with valid data creates a host" do
      combined_attrs = Map.merge(@valid_attrs, host_setup())

      assert {:ok, %Host{} = host} = Metademo.create_host(combined_attrs)
      assert host.hostname == "some hostname"
      assert host.inserted_by == "some inserted_by"
      assert host.ip_address == "some ip_address"
      assert host.is_active == 42
      assert host.updated_by == "some updated_by"
      assert host.uuid == "some uuid"
    end

    test "create_host/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Metademo.create_host(@invalid_attrs)
    end

    test "update_host/2 with valid data updates the host" do
      host = host_fixture()
      assert {:ok, %Host{} = host} = Metademo.update_host(host, @update_attrs)
      assert host.hostname == "some updated hostname"
      assert host.inserted_by == "some updated inserted_by"
      assert host.ip_address == "some updated ip_address"
      assert host.is_active == 43
      assert host.updated_by == "some updated updated_by"
      assert host.uuid == "some updated uuid"
    end

    test "update_host/2 with invalid data returns error changeset" do
      host = host_fixture()
      assert {:error, %Ecto.Changeset{}} = Metademo.update_host(host, @invalid_attrs)
      assert host == Metademo.get_host!(host.id)
    end

    test "delete_host/1 deletes the host" do
      host = host_fixture()
      assert {:ok, %Host{}} = Metademo.delete_host(host)
      assert_raise Ecto.NoResultsError, fn -> Metademo.get_host!(host.id) end
    end

    test "change_host/1 returns a host changeset" do
      host = host_fixture()
      assert %Ecto.Changeset{} = Metademo.change_host(host)
    end
  end

  describe "host_attrs" do
    alias MetademoJaxex.Metademo.HostAttr

    @valid_attrs %{
      inserted_by: "some inserted_by",
      is_active: 42,
      name: "some name",
      updated_by: "some updated_by",
      value: "some value"
    }
    @update_attrs %{
      inserted_by: "some updated inserted_by",
      is_active: 43,
      name: "some updated name",
      updated_by: "some updated updated_by",
      value: "some updated value"
    }
    @invalid_attrs %{inserted_by: nil, is_active: nil, name: nil, updated_by: nil, value: nil}

    def ha_host_setup() do
      host_role =
        host_role_fixture(%{
          name: "Web Server",
          description: "use for hosts that are a web server",
          is_active: 1,
          inserted_by: "Seeder",
          updated_by: "Seeder"
        })

      host_type =
        host_type_fixture(%{
          name: "VIRTUAL",
          description: "use this type for virtual machines type hosts",
          is_active: 1,
          inserted_by: "Seeder",
          updated_by: "Seeder"
        })

      datacenter =
        datacenter_fixture(%{
          code: "AUS",
          name: "Austin",
          is_active: 1,
          pattern: "",
          inserted_by: "Seeder",
          updated_by: "Seeder"
        })

      base_host = %{
        hostname: "some hostname",
        inserted_by: "some inserted_by",
        ip_address: "some ip_address",
        is_active: 42,
        updated_by: "some updated_by",
        uuid: "some uuid"
      }

      ids = %{
        datacenter_id: datacenter.id,
        host_role_id: host_role.id,
        host_type_id: host_type.id
      }

      attrs = Map.merge(base_host, ids)

      Metademo.create_host(attrs)
    end

    def host_attr_fixture(attrs \\ %{}) do
      {:ok, host} = ha_host_setup()

      {:ok, host_attr} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Metademo.create_host_attr(host)

      host_attr
    end

    test "list_host_attrs/0 returns all host_attrs" do
      host_attr = host_attr_fixture()
      assert Metademo.list_host_attrs() == [host_attr]
    end

    test "get_host_attr!/1 returns the host_attr with given id" do
      host_attr = host_attr_fixture()
      assert Metademo.get_host_attr!(host_attr.id) == host_attr
    end

    test "create_host_attr/1 with valid data creates a host_attr" do
      {:ok, host} = ha_host_setup()

      assert {:ok, %HostAttr{} = host_attr} = Metademo.create_host_attr(@valid_attrs, host)
      assert host_attr.inserted_by == "some inserted_by"
      assert host_attr.is_active == 42
      assert host_attr.name == "some name"
      assert host_attr.updated_by == "some updated_by"
      assert host_attr.value == "some value"
    end

    test "create_host_attr/1 with invalid data returns error changeset" do
      {:ok, host} = ha_host_setup()

      assert {:error, %Ecto.Changeset{}} = Metademo.create_host_attr(@invalid_attrs, host)
    end

    test "update_host_attr/2 with valid data updates the host_attr" do
      host_attr = host_attr_fixture()

      assert {:ok, %HostAttr{} = host_attr} = Metademo.update_host_attr(host_attr, @update_attrs)
      assert host_attr.inserted_by == "some updated inserted_by"
      assert host_attr.is_active == 43
      assert host_attr.name == "some updated name"
      assert host_attr.updated_by == "some updated updated_by"
      assert host_attr.value == "some updated value"
    end

    test "update_host_attr/2 with invalid data returns error changeset" do
      host_attr = host_attr_fixture()
      assert {:error, %Ecto.Changeset{}} = Metademo.update_host_attr(host_attr, @invalid_attrs)
      assert host_attr == Metademo.get_host_attr!(host_attr.id)
    end

    test "delete_host_attr/1 deletes the host_attr" do
      host_attr = host_attr_fixture()
      assert {:ok, %HostAttr{}} = Metademo.delete_host_attr(host_attr)
      assert_raise Ecto.NoResultsError, fn -> Metademo.get_host_attr!(host_attr.id) end
    end

    test "change_host_attr/1 returns a host_attr changeset" do
      host_attr = host_attr_fixture()
      assert %Ecto.Changeset{} = Metademo.change_host_attr(host_attr)
    end
  end

  describe "pod" do
    alias MetademoJaxex.Metademo.Pod

    @valid_attrs %{
      db_host: "some db_host",
      db_host2: "some db_host2",
      inserted_by: "some inserted_by",
      is_active: 42,
      name: "some name",
      physical_podname: "some physical_podname",
      port: "some port",
      sid: "some sid",
      type: "some type",
      updated_by: "some updated_by",
      zone: "some zone"
    }
    @update_attrs %{
      db_host: "some updated db_host",
      db_host2: "some updated db_host2",
      inserted_by: "some updated inserted_by",
      is_active: 43,
      name: "some updated name",
      physical_podname: "some updated physical_podname",
      port: "some updated port",
      sid: "some updated sid",
      type: "some updated type",
      updated_by: "some updated updated_by",
      zone: "some updated zone"
    }
    @invalid_attrs %{
      db_host: nil,
      db_host2: nil,
      inserted_by: nil,
      is_active: nil,
      name: nil,
      physical_podname: nil,
      port: nil,
      sid: nil,
      type: nil,
      updated_by: nil,
      zone: nil
    }

    def datacenter_setup() do
      datacenter =
        datacenter_fixture(%{
          code: "AUS",
          name: "Austin",
          is_active: 1,
          pattern: "",
          inserted_by: "Seeder",
          updated_by: "Seeder"
        })

      %{datacenter_id: datacenter.id}
    end

    def pod_fixture(attrs \\ %{}) do
      {:ok, pod} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Map.merge(datacenter_setup())
        |> Metademo.create_pod()

      pod
    end

    test "list_pod/0 returns all pod" do
      pod = pod_fixture()
      assert Metademo.list_pod() == [pod]
    end

    test "get_pod!/1 returns the pod with given id" do
      pod = pod_fixture()
      assert Metademo.get_pod!(pod.id) == pod
    end

    test "create_pod/1 with valid data creates a pod" do
      combined_attrs = Map.merge(@valid_attrs, datacenter_setup())

      assert {:ok, %Pod{} = pod} = Metademo.create_pod(combined_attrs)
      assert pod.db_host == "some db_host"
      assert pod.db_host2 == "some db_host2"
      assert pod.inserted_by == "some inserted_by"
      assert pod.is_active == 42
      assert pod.name == "some name"
      assert pod.physical_podname == "some physical_podname"
      assert pod.port == "some port"
      assert pod.sid == "some sid"
      assert pod.type == "some type"
      assert pod.updated_by == "some updated_by"
      assert pod.zone == "some zone"
    end

    test "create_pod/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Metademo.create_pod(@invalid_attrs)
    end

    test "update_pod/2 with valid data updates the pod" do
      pod = pod_fixture()
      assert {:ok, %Pod{} = pod} = Metademo.update_pod(pod, @update_attrs)
      assert pod.db_host == "some updated db_host"
      assert pod.db_host2 == "some updated db_host2"
      assert pod.inserted_by == "some updated inserted_by"
      assert pod.is_active == 43
      assert pod.name == "some updated name"
      assert pod.physical_podname == "some updated physical_podname"
      assert pod.port == "some updated port"
      assert pod.sid == "some updated sid"
      assert pod.type == "some updated type"
      assert pod.updated_by == "some updated updated_by"
      assert pod.zone == "some updated zone"
    end

    test "update_pod/2 with invalid data returns error changeset" do
      pod = pod_fixture()
      assert {:error, %Ecto.Changeset{}} = Metademo.update_pod(pod, @invalid_attrs)
      assert pod == Metademo.get_pod!(pod.id)
    end

    test "delete_pod/1 deletes the pod" do
      pod = pod_fixture()
      assert {:ok, %Pod{}} = Metademo.delete_pod(pod)
      assert_raise Ecto.NoResultsError, fn -> Metademo.get_pod!(pod.id) end
    end

    test "change_pod/1 returns a pod changeset" do
      pod = pod_fixture()
      assert %Ecto.Changeset{} = Metademo.change_pod(pod)
    end
  end

  describe "pod_members" do
    alias MetademoJaxex.Metademo.PodMember

    @valid_attrs %{
      host_id: 42,
      inserted_by: "some inserted_by",
      is_active: 1,
      pod_id: 42,
      updated_by: "some updated_by"
    }
    @update_attrs %{
      host_id: 43,
      inserted_by: "some updated inserted_by",
      is_active: 0,
      pod_id: 43,
      updated_by: "some updated updated_by"
    }
    @invalid_attrs %{host_id: nil, inserted_by: nil, is_active: nil, pod_id: nil, updated_by: nil}

    def pod_member_setup() do
      host_role =
        Repo.insert!(%HostRole{
          name: "Web Server",
          description: "use for hosts that are a web server",
          is_active: 1,
          inserted_by: "Test",
          updated_by: "Test"
        })

      host_type =
        Repo.insert!(%HostType{
          name: "VIRTUAL",
          description: "use this type for virtual machines type hosts",
          is_active: 1,
          inserted_by: "Test",
          updated_by: "Test"
        })

      datacenter =
        Repo.insert!(%Datacenter{
          code: "AUS",
          name: "Austin",
          is_active: 1,
          pattern: "",
          inserted_by: "Seeder",
          updated_by: "Seeder"
        })

      host_one =
        Repo.insert!(%Host{
          hostname: "web.somehost01.com",
          host_role_id: host_role.id,
          host_type_id: host_type.id,
          datacenter_id: datacenter.id,
          is_active: 1,
          uuid: "ABCD-EFGH-IJKL",
          ip_address: "192.168.1.4",
          inserted_by: "Test",
          updated_by: "Test"
        })

      pod_one =
        Repo.insert!(%Pod{
          name: "PodOne",
          physical_podname: "PodOne-DG1",
          is_active: 1,
          type: "Basion",
          zone: "MW",
          datacenter_id: datacenter.id,
          port: "1532",
          sid: "bobots",
          db_host: "dbsrv01",
          db_host2: "dbsrv02",
          inserted_by: "Test",
          updated_by: "Test"
        })

      %{pod_id: pod_one.id, host_id: host_one.id}
    end

    def pod_member_fixture(attrs \\ %{}) do
      {:ok, pod_member} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Map.merge(pod_member_setup())
        |> Metademo.create_pod_member()

      pod_member
    end

    test "list_pod_members/0 returns all pod_members" do
      pod_member = pod_member_fixture()
      assert Metademo.list_pod_members() == [pod_member]
    end

    test "get_pod_member!/1 returns the pod_member with given id" do
      pod_member = pod_member_fixture()
      assert Metademo.get_pod_member!(pod_member.id) == pod_member
    end

    test "create_pod_member/1 with valid data creates a pod_member" do
      combined_attrs = Map.merge(@valid_attrs, pod_member_setup())

      assert {:ok, %PodMember{} = pod_member} = Metademo.create_pod_member(combined_attrs)
      assert pod_member.host_id != nil
      assert pod_member.inserted_by == "some inserted_by"
      assert pod_member.is_active == 1
      assert pod_member.pod_id !== nil
      assert pod_member.updated_by == "some updated_by"
    end

    test "create_pod_member/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Metademo.create_pod_member(@invalid_attrs)
    end

    test "update_pod_member/2 with valid data updates the pod_member" do
      pod_member = pod_member_fixture()
      pod_host_info = %{pod_id: pod_member.pod_id, host_id: pod_member.host_id}
      combined_attrs = Map.merge(@update_attrs, pod_host_info)

      assert {:ok, %PodMember{} = pod_member} =
               Metademo.update_pod_member(pod_member, combined_attrs)

      assert pod_member.host_id != nil
      assert pod_member.inserted_by == "some updated inserted_by"
      assert pod_member.is_active == 0
      assert pod_member.pod_id != nil
      assert pod_member.updated_by == "some updated updated_by"
    end

    test "update_pod_member/2 with invalid data returns error changeset" do
      pod_member = pod_member_fixture()
      assert {:error, %Ecto.Changeset{}} = Metademo.update_pod_member(pod_member, @invalid_attrs)
      assert pod_member == Metademo.get_pod_member!(pod_member.id)
    end

    test "delete_pod_member/1 deletes the pod_member" do
      pod_member = pod_member_fixture()
      assert {:ok, %PodMember{}} = Metademo.delete_pod_member(pod_member)
      assert_raise Ecto.NoResultsError, fn -> Metademo.get_pod_member!(pod_member.id) end
    end

    test "change_pod_member/1 returns a pod_member changeset" do
      pod_member = pod_member_fixture()
      assert %Ecto.Changeset{} = Metademo.change_pod_member(pod_member)
    end
  end
end
