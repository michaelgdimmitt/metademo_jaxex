alias MetademoJaxex.{
  Repo,
  Metademo.Datacenter,
  Metademo.Host,
  Metademo.HostRole,
  Metademo.HostType,
  Metademo.Pod,
  Metademo.PodMember
}

import Ecto.Query
