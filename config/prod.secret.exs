use Mix.Config

database_url =
  System.get_env("DATABASE_URL") ||
    raise """
    environment variable DATABASE_URL is missing.
    For example: ecto://USER:PASS@HOST/DATABASE
    """

config :metademo_jaxex, MetademoJaxex.Repo,
  adapter: Ecto.Adapters.Postgres,
  ssl: true,
  url: database_url,
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "2")

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    raise """
    environment variable SECRET_KEY_BASE is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :metademo_jaxex, MetademoJaxexWeb.Endpoint,
  http: [:inet6, port: String.to_integer(System.get_env("PORT") || "80")],
  # http: [:inet6, port: String.to_integer(System.get_env("PORT") || "4000")],
  url: [host: "sympathetic-coordinated-fawn.gigalixirapp.com", port: 80],
  # url: [host: System.get_env("APP_NAME") <> ".gigalixirapp.com", port: 80],
  secret_key_base: secret_key_base,
  server: true
