use Mix.Config

# Configure your database
config :metademo_jaxex, MetademoJaxex.Repo,
  username: "postgres",
  password: "Supp0rt",
  database: "metademo_jaxex_test",
  hostname: "postgres",
  port: 5432,
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :metademo_jaxex, MetademoJaxexWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
