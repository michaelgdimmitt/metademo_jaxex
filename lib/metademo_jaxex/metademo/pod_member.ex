defmodule MetademoJaxex.Metademo.PodMember do
  use Ecto.Schema
  import Ecto.Changeset

  schema "pod_members" do
    field :inserted_by, :string
    field :is_active, :integer
    field :updated_by, :string

    timestamps()

    belongs_to :pod, MetademoJaxex.Metademo.Pod
    belongs_to :host, MetademoJaxex.Metademo.Host
  end

  @doc false
  def changeset(pod_member, attrs) do
    pod_member
    |> cast(attrs, [:pod_id, :host_id, :is_active, :inserted_by, :updated_by])
    |> validate_required([:pod_id, :host_id, :is_active, :inserted_by, :updated_by])
  end
end
