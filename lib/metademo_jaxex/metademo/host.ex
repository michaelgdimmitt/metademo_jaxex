defmodule MetademoJaxex.Metademo.Host do
  use Ecto.Schema
  import Ecto.Changeset

  schema "hosts" do
    field :hostname, :string
    field :inserted_by, :string
    field :ip_address, :string
    field :is_active, :integer
    field :updated_by, :string
    field :uuid, :string

    timestamps()

    belongs_to :host_role, MetademoJaxex.Metademo.HostRole
    belongs_to :host_type, MetademoJaxex.Metademo.HostType
    belongs_to :datacenter, MetademoJaxex.Metademo.Datacenter

    has_many :host_attrs, MetademoJaxex.Metademo.HostAttr

    # both support association for pod -> hosts via pod_members
    has_one :pod_member, MetademoJaxex.Metademo.PodMember
    has_one :pod, through: [:pod_member, :pod]
  end

  @doc false
  def changeset(host, attrs) do
    host
    |> cast(attrs, [
      :hostname,
      :is_active,
      :inserted_by,
      :updated_by,
      :uuid,
      :ip_address,
      :host_role_id,
      :host_type_id,
      :datacenter_id
    ])
    |> validate_required([
      :hostname,
      :is_active,
      :inserted_by,
      :updated_by,
      :uuid,
      :ip_address,
      :host_role_id,
      :host_type_id,
      :datacenter_id
    ])
  end
end
