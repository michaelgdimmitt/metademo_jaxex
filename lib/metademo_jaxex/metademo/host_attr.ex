defmodule MetademoJaxex.Metademo.HostAttr do
  use Ecto.Schema
  import Ecto.Changeset

  schema "host_attrs" do
    field :inserted_by, :string
    field :is_active, :integer
    field :name, :string
    field :updated_by, :string
    field :value, :string

    timestamps()

    belongs_to :host, MetademoJaxex.Metademo.Host
  end

  @doc false
  def changeset(host_attr, attrs) do
    host_attr
    |> cast(attrs, [:host_id, :name, :value, :is_active, :inserted_by, :updated_by])
    |> validate_required([:host_id, :name, :value, :is_active, :inserted_by, :updated_by])
  end
end
