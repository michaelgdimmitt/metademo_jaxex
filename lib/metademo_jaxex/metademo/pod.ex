defmodule MetademoJaxex.Metademo.Pod do
  use Ecto.Schema
  import Ecto.Changeset

  schema "pods" do
    field :name, :string
    field :db_host, :string
    field :db_host2, :string
    field :inserted_by, :string
    field :is_active, :integer
    field :physical_podname, :string
    field :port, :string
    field :sid, :string
    field :type, :string
    field :updated_by, :string
    field :zone, :string

    timestamps()

    belongs_to :datacenter, MetademoJaxex.Metademo.Datacenter

    # both support association for pod -> hosts via pod_members
    has_many :pod_members, MetademoJaxex.Metademo.PodMember
    has_many :hosts, through: [:pod_members, :host]
  end

  @doc false
  def changeset(pod, attrs) do
    pod
    |> cast(attrs, [
      :name,
      :physical_podname,
      :type,
      :db_host,
      :db_host2,
      :sid,
      :port,
      :zone,
      :is_active,
      :inserted_by,
      :updated_by,
      :datacenter_id
    ])
    |> validate_required([
      :name,
      :physical_podname,
      :type,
      :db_host,
      :db_host2,
      :sid,
      :port,
      :zone,
      :is_active,
      :inserted_by,
      :updated_by,
      :datacenter_id
    ])
  end
end
