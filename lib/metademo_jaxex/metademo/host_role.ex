defmodule MetademoJaxex.Metademo.HostRole do
  use Ecto.Schema
  import Ecto.Changeset

  schema "host_roles" do
    field :description, :string
    field :inserted_by, :string
    field :is_active, :integer
    field :name, :string
    field :updated_by, :string

    timestamps()
  end

  @doc false
  def changeset(host_role, attrs) do
    host_role
    |> cast(attrs, [:name, :description, :is_active, :inserted_by, :updated_by])
    |> validate_required([:name, :description, :is_active, :inserted_by, :updated_by])
  end
end
