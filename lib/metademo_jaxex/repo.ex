defmodule MetademoJaxex.Repo do
  use Ecto.Repo,
    otp_app: :metademo_jaxex,
    adapter: Ecto.Adapters.Postgres
end
