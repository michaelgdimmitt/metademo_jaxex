defmodule MetademoJaxexWeb.PodController do
  use MetademoJaxexWeb, :controller

  alias MetademoJaxex.Metademo
  alias MetademoJaxex.Metademo.Pod

  action_fallback MetademoJaxexWeb.FallbackController

  def index(conn, _params) do
    pod = Metademo.list_pod()
    render(conn, "index.json", pod: pod)
  end

  def create(conn, %{"pod" => pod_params}) do
    with {:ok, %Pod{} = pod} <- Metademo.create_pod(pod_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.pod_path(conn, :show, pod))
      |> render("show.json", pod: pod)
    end
  end

  def show(conn, %{"id" => id}) do
    pod = Metademo.get_pod!(id)
    render(conn, "show.json", pod: pod)
  end

  def update(conn, %{"id" => id, "pod" => pod_params}) do
    pod = Metademo.get_pod!(id)

    with {:ok, %Pod{} = pod} <- Metademo.update_pod(pod, pod_params) do
      render(conn, "show.json", pod: pod)
    end
  end

  def delete(conn, %{"id" => id}) do
    pod = Metademo.get_pod!(id)

    with {:ok, %Pod{}} <- Metademo.delete_pod(pod) do
      send_resp(conn, :no_content, "")
    end
  end
end
