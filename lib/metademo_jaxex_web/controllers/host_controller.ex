defmodule MetademoJaxexWeb.HostController do
  use MetademoJaxexWeb, :controller

  alias MetademoJaxex.Metademo
  alias MetademoJaxex.Metademo.Host

  action_fallback MetademoJaxexWeb.FallbackController

  def index(conn, _params) do
    hosts = Metademo.list_hosts()
    render(conn, "index.json", hosts: hosts)
  end

  def create(conn, %{"host" => host_params}) do
    with {:ok, %Host{} = host} <- Metademo.create_host(host_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.host_path(conn, :show, host))
      |> render("show.json", host: host)
    end
  end

  def show(conn, %{"id" => id}) do
    host = Metademo.get_host!(id)
    render(conn, "show.json", host: host)
  end

  def update(conn, %{"id" => id, "host" => host_params}) do
    host = Metademo.get_host!(id)

    with {:ok, %Host{} = host} <- Metademo.update_host(host, host_params) do
      render(conn, "show.json", host: host)
    end
  end

  def delete(conn, %{"id" => id}) do
    host = Metademo.get_host!(id)

    with {:ok, %Host{}} <- Metademo.delete_host(host) do
      send_resp(conn, :no_content, "")
    end
  end
end
