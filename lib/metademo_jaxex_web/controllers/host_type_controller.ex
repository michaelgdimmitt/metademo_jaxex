defmodule MetademoJaxexWeb.HostTypeController do
  use MetademoJaxexWeb, :controller

  alias MetademoJaxex.Metademo
  alias MetademoJaxex.Metademo.HostType

  action_fallback MetademoJaxexWeb.FallbackController

  def index(conn, _params) do
    host_types = Metademo.list_host_types()
    render(conn, "index.json", host_types: host_types)
  end

  def create(conn, %{"host_type" => host_type_params}) do
    with {:ok, %HostType{} = host_type} <- Metademo.create_host_type(host_type_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.host_type_path(conn, :show, host_type))
      |> render("show.json", host_type: host_type)
    end
  end

  def show(conn, %{"id" => id}) do
    host_type = Metademo.get_host_type!(id)
    render(conn, "show.json", host_type: host_type)
  end

  def update(conn, %{"id" => id, "host_type" => host_type_params}) do
    host_type = Metademo.get_host_type!(id)

    with {:ok, %HostType{} = host_type} <- Metademo.update_host_type(host_type, host_type_params) do
      render(conn, "show.json", host_type: host_type)
    end
  end

  def delete(conn, %{"id" => id}) do
    host_type = Metademo.get_host_type!(id)

    with {:ok, %HostType{}} <- Metademo.delete_host_type(host_type) do
      send_resp(conn, :no_content, "")
    end
  end
end
