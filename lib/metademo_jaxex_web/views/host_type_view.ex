defmodule MetademoJaxexWeb.HostTypeView do
  use MetademoJaxexWeb, :view
  alias MetademoJaxexWeb.HostTypeView

  def render("index.json", %{host_types: host_types}) do
    %{data: render_many(host_types, HostTypeView, "host_type.json")}
  end

  def render("show.json", %{host_type: host_type}) do
    %{data: render_one(host_type, HostTypeView, "host_type.json")}
  end

  def render("host_type.json", %{host_type: host_type}) do
    %{
      id: host_type.id,
      name: host_type.name,
      description: host_type.description,
      is_active: host_type.is_active,
      inserted_by: host_type.inserted_by,
      updated_by: host_type.updated_by
    }
  end
end
