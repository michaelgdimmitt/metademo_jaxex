defmodule MetademoJaxexWeb.HostView do
  use MetademoJaxexWeb, :view
  alias MetademoJaxexWeb.HostView

  def render("index.json", %{hosts: hosts}) do
    %{data: render_many(hosts, HostView, "host.json")}
  end

  def render("show.json", %{host: host}) do
    %{data: render_one(host, HostView, "host.json")}
  end

  def render("host.json", %{host: host}) do
    %{
      id: host.id,
      hostname: host.hostname,
      is_active: host.is_active,
      inserted_by: host.inserted_by,
      updated_by: host.updated_by,
      uuid: host.uuid,
      ip_address: host.ip_address
    }
  end
end
