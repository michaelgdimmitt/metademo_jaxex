defmodule MetademoJaxexWeb.DatacenterView do
  use MetademoJaxexWeb, :view
  alias MetademoJaxexWeb.DatacenterView

  def render("index.json", %{datacenters: datacenters}) do
    %{data: render_many(datacenters, DatacenterView, "datacenter.json")}
  end

  def render("show.json", %{datacenter: datacenter}) do
    %{data: render_one(datacenter, DatacenterView, "datacenter.json")}
  end

  def render("datacenter.json", %{datacenter: datacenter}) do
    %{
      id: datacenter.id,
      code: datacenter.code,
      name: datacenter.name,
      pattern: datacenter.pattern,
      inserted_by: datacenter.inserted_by,
      updated_by: datacenter.updated_by,
      is_active: datacenter.is_active
    }
  end
end
