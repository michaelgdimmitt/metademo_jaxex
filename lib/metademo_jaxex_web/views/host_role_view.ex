defmodule MetademoJaxexWeb.HostRoleView do
  use MetademoJaxexWeb, :view
  alias MetademoJaxexWeb.HostRoleView

  def render("index.json", %{host_roles: host_roles}) do
    %{data: render_many(host_roles, HostRoleView, "host_role.json")}
  end

  def render("show.json", %{host_role: host_role}) do
    %{data: render_one(host_role, HostRoleView, "host_role.json")}
  end

  def render("host_role.json", %{host_role: host_role}) do
    %{
      id: host_role.id,
      name: host_role.name,
      description: host_role.description,
      is_active: host_role.is_active,
      inserted_by: host_role.inserted_by,
      updated_by: host_role.updated_by
    }
  end
end
